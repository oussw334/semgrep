FROM alpine:latest
MAINTAINER ME

RUN apk add --update --no-cache \
            ca-certificates \
            libpcap \
            curl util-linux \
            libcurl \
            libxml2-utils \
            python3-dev \
            nmap \
            nmap-scripts \
            nikto \
            bind-tools \
            py3-pip \
            bash \
            bash-completion \
            util-linux \
            pciutils \
            usbutils \
            coreutils \
            binutils \
            findutils \
            grep \
            procps \
            drill \
            git \
            tzdata \
            cmake \
            make \
            musl-dev \
            gcc \
            gettext-dev \
            libintl \
            coreutils \
            libidn \
            openssl \
            libstdc++ \
            chromium \
            chromium-chromedriver \
            harfbuzz \
            nss \
            freetype \
            ttf-freefont \
            perl\
            perl-net-ssleay \
            python3-dev \
            htop \
            bind-tools \
            ruby-dev \
            ruby-bundler \
            libxslt-dev \
            libxml2-dev \
            g++ \
            texlive-xetex \
            texmf-dist-latexextra \
            && update-ca-certificates \
            && rm -rf /var/cache/apk/* \
            && apk add --no-cache --virtual .build-dependencies build-base curl-dev \
            && pip3 install --upgrade pip \
            && pip3 install pycurl \
            && apk del .build-dependencies \
            && pip3 install wfuzz


RUN python3 -m pip install semgrep
RUN pip3 install truffleHog
RUN pip3  install requests
RUN pip3 install -U Flask



